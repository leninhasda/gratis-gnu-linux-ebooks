GNU/Linux related e-Books: Available Free of Charge
====================================================

### GNU/Linux System Basics ###

* #### Absolute Beginners ####
    * [Learn Linux in 5 Days](https://courses.linuxtrainingacademy.com/itsfoss-ll5d/)¹
    * [Ultimate Linux Newbie Guide](https://linuxnewbieguide.org/ebook/)


* #### Beginners ####
    * [Introduction to Linux: A Hands on Guide](http://www.tldp.org/LDP/intro-linux/html/)
    * [Linux Fundamentals](http://linux-training.be/)
    * [GNU/Linux Basic](http://ftacademy.org/courses/gnu-linux-basic/)
    * [The Unix Workbench](https://leanpub.com/unix)


### GNU/Linux Command Line ###

* [The Linux Command Line](http://linuxcommand.org/)
* [FLOSS Manuals' Command Line](http://flossmanuals.net/command-line/) 
* [GNU/Linux Command-Line Tools Summary](http://www.tldp.org/LDP/GNU-Linux-Tools-Summary/html/)
* Bash Scripting
    * [Bash Guide for Beginners](http://www.tldp.org/LDP/Bash-Beginners-Guide/html/)
    * [Advanced Bash-Scripting Guide](http://www.tldp.org/LDP/abs/html/)


### GNU/Linux Development ###

* [Advanced Linux Programming](http://advancedlinuxprogramming.com/)


### GNU/Linux System Administration ###

* [GNU/Linux Advanced Administration](http://ftacademy.org/courses/gnu-linux-advanced/)
* [Linux System Administration](http://linux-training.be/)
* [Linux Servers](http://linux-training.be/)
* [Linux Storage](http://linux-training.be/)
* [Linux Secuirity](http://linux-training.be/)
* [Linux Networking](http://linux-training.be/)


### Distro Specific ###

* #### Debian GNU/Linux ####
    * [The Debian Administrator's Handbook](https://debian-handbook.info/)
    * [Debian Installation Guide](https://www.debian.org/releases/stable/installmanual)

* #### Ubuntu ####

    * [Getting Started with Ubuntu](http://ubuntu-manual.org/)
    * [Ubuntu Installation Guide](https://help.ubuntu.com/)
    * [Ubuntu Server Guide](https://help.ubuntu.com/)
    * [Ubuntu Packaging Guide](http://packaging.ubuntu.com/)
    * [Little Orange Ubuntu Book](https://leanpub.com/littleorangeubuntubook/)

* #### openSUSE ####
	* 

* #### Linux Mint ####

    * [Linux Mint User Guide](https://linuxmint.com/documentation.php)
    * [Just Tell Me Damnit](http://downtoearthlinux.com/resources/just-tell-me-damnit/)


### Linux Kernel ###

* [Linux Kernel in a Nutshell](http://www.kroah.com/lkn/)

### Notes ###

¹ This book is available gratis only for the readers of [It's FOSS](https://itsfoss.com/). Although, it has not to be verified in any way for downloading this book.

### Contributing ###

This e-Books are being listed by [Nave Nobel](http://thenanobel.gitlab.io), you can help me by sending a merge request [here](https://gitlab.com/thenanobel/gratis-gnu-linux-ebooks).